Ladda.bind('div:not(.progress-demo) button', {timeout: 2000});

var workoutMeta = [
    "warrior", 14, 8, "http://hasfit.com/warrior-90-day-workout-routine-exercise-program-fitness-schedule-work-out-plan/",
    "hero", 14, 8, "http://hasfit.com/hero-90-high-intensity-exercise-program/",
    "getInShape", 6, 8, "http://hasfit.com/30-day-fitness-challenge-exercise-program-workout-plan-to-get-in-shape/",
    ];
var numOfMetadata = 4;

$(document).ready(function () {
    $('.btn').click(function () {
        $("#resultHeader").empty();
        $("#result").empty();

        var l = Ladda.create(this);
        l.start();


        var randomtd;
        var selectedWorkout = getSelectedWorkout($("#workout").find("input:checked"));
        var indexOfSelectedWorkout = getIndexOfSelectedWorkout(selectedWorkout);
        var url =workoutMeta[indexOfSelectedWorkout + 3];
        $.get("redirect.php?url=" + url, function (data) {
            $page = $(data);
            var numOfRows = workoutMeta[indexOfSelectedWorkout + 1];
            var numOfColumns = workoutMeta[indexOfSelectedWorkout + 2];

            while (!isValid(randomtd)) {
                var randomNum = getRandomInt(1, numOfRows * numOfColumns);
                var actualNum = randomNum + 1;
                randomtd = $('table td', $page).eq(randomNum);
            }
            var week = Math.floor(randomNum/numOfColumns);
            var day = randomNum%numOfColumns;
            randomtd.css({"border": "0px"});
            l.stop();
            $("#resultHeader").html("<p >A workout from <b>Week "
            + week + ", Day " + day + "</b> of the <b>"
            + "<a href=\""+ url + "\" target=\"_blank\">"
            + getWorkoutDescription(selectedWorkout)
            + "</a></b></p>");
            $("#result").html(randomtd);
        });
    });
});

function getSelectedWorkout(selectedWorkout) {
    var selectedWorkoutText = selectedWorkout.val();
    if (selectedWorkoutText === "surpriseMe") {
        var rnd = getRandomInt(0, workoutMeta.length / numOfMetadata - 1);
        return workoutMeta[rnd * numOfMetadata];
    }
    return selectedWorkoutText;
}

function getIndexOfSelectedWorkout(selectedWorkout) {
    for (var i = 0; i < workoutMeta.length; i += numOfMetadata) {
        if (workoutMeta[i] == selectedWorkout) {
            return i;
        }
    }
    alert("Something bad happened!");
}

function getWorkoutDescription(selectedWorkout)
{
    return $("#workout").find("#" + selectedWorkout + " + label").text();
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isValid(randomtd) {
    return (!(typeof randomtd === 'undefined'))
        && randomtd.text().indexOf("OFF") == -1
        && randomtd.text().indexOf("Week") == -1
        && randomtd.text() != ""
        && randomtd.text().indexOf("Day") == -1;
}
